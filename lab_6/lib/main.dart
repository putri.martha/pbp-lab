import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'To Do List',
      theme: ThemeData(
        brightness: Brightness.dark,
        primaryColor: Colors.blueGrey,
        fontFamily: 'Sans Serif',
        primarySwatch: Colors.blue,
      ),
      // home: Scaffold(title: 'To Do List'),
      home: Scaffold(
        appBar: AppBar(
          title: Text('To Do List'),
        ),
        body: const MyHomePage(),
      )
    );
  }
}

class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key}) : super(key: key);

//   // This widget is the home page of your application. It is stateful, meaning
//   // that it has a State object (defined below) that contains fields that affect
//   // how it looks.

//   // This class is the configuration for the state. It holds the values (in this
//   // case the title) provided by the parent (in this case the App widget) and
//   // used by the build method of the State. Fields in a Widget subclass are
//   // always marked "final".

//   final String title;

//   @override
//   State<MyHomePage> createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {


  @override
  Widget build(BuildContext context) {
    return Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            // const ListTile(
            //   leading: Icon(Icons.album),
            //   title: Text('Add Your Task'),
            // ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal:100, vertical: 10),
              child: Text('Add New Task')
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal:100, vertical: 10),
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder()
                )
              )
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal:100, vertical: 10),
              child: Text('Due Date')
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal:100, vertical: 10),
              child: TextField(
                decoration: InputDecoration(
                  border: OutlineInputBorder()
                )
              )
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal:100, vertical: 10),
              child: SizedBox(
                height: 20,
                width: 100,
                child: TextButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.blueAccent),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                  ),
                  onPressed: () {},
                  child: Text('ADD'),
                )
              )
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal:100, vertical: 10),
              child: SizedBox(
                height: 20,
                width: 100,
                child: TextButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.blueAccent),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                  ),
                  onPressed: () {},
                  child: Text('SORT'),
                ),
              )
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal:100, vertical: 10),
              child: SizedBox(
                height: 20,
                width: 200,
                child: TextButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.blueAccent),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                  ),
                  onPressed: () {},
                  child: Text('DELETE COMPLETED'),
                ),
              )
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal:100, vertical: 10),
              child: SizedBox(
                height: 20,
                width: 200,
                child: TextButton(
                  style: ButtonStyle(
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.blueAccent),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                  ),
                  onPressed: () {},
                  child: Text('DELETE ALL'),
                ),
              )
            )
          ],
        );
    
  }
}

