1. Apakah perbedaan antara JSON dan XML?
JSON:
- format yang ditulis dalam javascript
- didesain menjadi self-describing sehingga mudah dimengerti
- data disimpan dalam bentuk key dan value
- mendukung berbagai tipe data diantaranya string, number, array, boolean
- tidak mendukug namespaces, komentar dan metadata
- objek pada JSON memiliki type
- tidak dapat melakukan pemrosesan atau perhitungan apa pun
- JSON didukung oleh hampir seluruh browser

XML:
- penulisan mirip HTML, membutuhkan tag untuk mendefinisikan elemen dan harus punya root elemen
- didesain menjadi self-descriptive sehingga informasi dari data dapat dimengerti
- dokumen membentuk struktur tre
- tipe data hanya string
- mendukung namespaces, komentar dan metadata
- data pada XML typeless
- dapat melakukan pemrosesan dan pemformatan dokumen dan objek
- browser yang mendukung XML terbatas
referensi:
https://www.monitorteknologi.com/perbedaan-json-dan-xml/
https://www.guru99.com/json-tutorial-example.html


2. Apakah perbedaan antara HTML dan XML?
HTML:
- tidak case sensitive
- tidak menyimpan/carry data, hanya menyajikan/menampilkan data
- error kecil dapat diabaikan
- closing tag tidak terlalu dibutuhkan
- whitescpace tidak dipertahakan
- tag digunakan untuk tampilin data
- tag sudah didefinisikan

XML:
- case sensitive
- menyimpan data, membawa dari/ke database
- tidak boleh ada error
- harus ada closing tag
- meng-ignore whitespace
- tag untuk mendeskripsikan data
- tag dapat didefinisikan sesuai kebutuhan pengguna (fleksibel)

referensi:
https://www.upgrad.com/blog/html-vs-xml/ 
https://www.guru99.com/xml-vs-html-difference.html
https://www.geeksforgeeks.org/html-vs-xml/