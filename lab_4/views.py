from django.http.response import HttpResponse, HttpResponseRedirect
from django.http import response
from django.shortcuts import render
from lab_2.models import Note
from .forms import NoteForm

# Create your views here.

def index(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    form = NoteForm(request.POST or None)
    if (request.method == 'POST'):
        if (form.is_valid()):
            form.save()
            return HttpResponseRedirect('/lab-4')
    return render(request, 'lab4_form.html', {'form' : form})

def note_list(request):
    notes = Note.objects.all()
    response = {'notes': notes}
    return render(request, 'lab4_notelist.html', response)